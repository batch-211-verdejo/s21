console.log("Hello World!");

// An array in programming is simply a list of data

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];

// Arrays
/* 
    Arrays are used to store multiple values in a single variable
    They are declared using square backets ([]) also known as "Array Literals"

    Syntax:

    let/const arrayName = [elementA, elementB, elementC...]

*/

// Common examples of arrays

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway", "Toshiba", "Fujitsu"];

console.log(grades);
console.log(computerBrands);

// Possible use of an array but it is not recommended

let mixedArr = [12, "Asus", null, undefined, {}];

console.log(mixedArr);

// Alternative way to write arrays
let myTasks = [
    "drink html",
    "eat javascript",
    "inhale css",
    "bake sass",

];

console.log(myTasks);

// Mini-activity

let tasks = ["Read business news", "Work-out", "Cook a meal", "Read technical charts", "practice coding"];

let capitalCities = ["Manila", "Brasilia", "Beijing", "Cairo"];

console.log(tasks);
console.log(capitalCities);

// Mini-activity end


let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities);

// Length property
// The .length property allows us to get and set the total number of items in an array

console.log(myTasks.length); //4
console.log(cities.length); //3

let blankArr = [];
console.log(blankArr.length); //0

// length property can also be used with strings
// some array methods and properties can also be used with strings

let fullName = "Jamie Noble";
console.log(fullName.length);

// length property can also set the total number of items in an array, meaning we can actually DELETE the last item in the array or shorten the array by simply updating the length property of an array
myTasks.length = myTasks.length-1;
console.log(myTasks.length);
console.log(myTasks);

// another example using decrementation
cities.length--;
console.log(cities);

fullName.length = fullName.length-1;
console.log(fullName.length);
fullName.length--;
console.log(fullName);

let theBeatles = ["John", "Paul", "Ringo", "George"];
// theBeatles.length++;

theBeatles[4] = "Cardo";
theBeatles[theBeatles.length] = "Ely";
theBeatles[theBeatles.length] = "Chito";
theBeatles[theBeatles.length] = "MJ";
console.log(theBeatles);

// Mini-activity 2
let theTrainers = ["Ash"];

function addTrainers(trainer) {
    theTrainers[theTrainers.length] = trainer;    
};

addTrainers("Misty");
addTrainers("Brock");
console.log(theTrainers);

// Mini-activity 2 end

// Reading from Arrays
/* 
    - Accessing array elements is one of the more common tasks that we do with an array
    - This can be done through the use of array indexes
    - Each element in an array is associated with its own index/number

    - the reason an array starts with 0 is due to how the languange is designed

    Syntax
        arrayName[index];

*/

console.log(grades[0]); //98.5
console.log(computerBrands[3]); //Neo
// Accessing an array element that does not exist will return "undefined"
console.log(grades[20]); //undefined

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];

console.log(lakersLegends[1]); //Shaq
console.log(lakersLegends[3]); //Magic

let currentLaker = lakersLegends[2];
console.log(currentLaker);
console.log(currentLaker);

console.log("Array before reassignment");
console.log(lakersLegends);
lakersLegends[2] = "Pau Gasol";
console.log("Array after reassignment");
console.log(lakersLegends);


// Mini-activity 3

    function findBlackMamba(index) {
        return lakersLegends[index];
    };
    let blackMamba = findBlackMamba(0);
    console.log(blackMamba);

// Mini-activity 3 end

// Mini-activity 4

    let favoriteFoods = [
        "Beef Sinigang",
        "Ramen",
        "Hamburger",
        "Bulalo",
        "Pizza"
    ];

    console.log(favoriteFoods)

    favoriteFoods[3] = "Shawarma";
    favoriteFoods[4] = "Sisig";

// Accessing the last element of an array

let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];

let lastElementIndex = bullsLegends.length-1;
console.log(lastElementIndex); //4
console.log(bullsLegends.length);

console.log(bullsLegends[lastElementIndex]);

console.log(bullsLegends[bullsLegends.length-1]);

// Adding items into the Array
let newArr = [];
console.log(newArr[0]);

newArr[0] = "Cloud Strife";
console.log(newArr);

console.log(newArr[1]);
newArr[1] = "Tifa Lockhart";
console.log(newArr);

// newArr[newArr.length-1] = "Aerith Gainsborough";

newArr[newArr.length] = "Barret Wallace"
console.log(newArr);

// Looping over an Array

for(let index = 0; index < newArr.length; index++) {
    console.log(newArr[index]);
}

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++) {

    if(numArr[index] % 5 === 0) {
        console.log(numArr[index] + " is divisible by 5")
    }
    else {
        console.log(numArr[index] + " is not divisible by 5");
    }
};

// Multidimensional Arrays

/* 
    - Multidimensional Arrays are useful for storing complex data structures
    - a practical application of this is to help visualize/create real world objects

*/

let chessBoard = [

    ["a1", "b1", "c1", "d1", "e1", "f1", "g1", "h1"],
    ["a2", "b2", "c2", "d2", "e2", "f2", "g2", "h2"],
    ["a3", "b3", "c3", "d3", "e3", "f3", "g3", "h3"],
    ["a4", "b4", "c4", "d4", "e4", "f4", "g4", "h4"],
    ["a5", "b5", "c5", "d5", "e5", "f5", "g5", "h5"],
    ["a6", "b6", "c6", "d6", "e6", "f6", "g6", "h6"],
    ["a7", "b7", "c7", "d7", "e7", "f7", "g7", "h7"],
    ["a8", "b8", "c8", "d8", "e8", "f8", "g8", "h8"]

];

console.log(chessBoard);

console.log(chessBoard[1][4]); //e2
console.log("Pawn moves to: " + chessBoard[1][5]); //f2

// Mini activity 5

console.log(chessBoard[7][0]);
console.log(chessBoard[5][7]);

// Mini activity 5 end